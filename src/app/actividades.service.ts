import { Actividad } from './actividad';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { RespuestaJSON } from './RespuestaJSON';

const headers = new HttpHeaders({
  'Content-Type': 'application/json; charset=utf-8'
});

const httpOptions = {
  headers: headers
};

/*new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token',
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, HEAD, OPTIONS'
  })*/

const httpOptionsDelete = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': 'localhost:8080',
    'Authorization': 'my-auth-token',
  })
};

@Injectable({
  providedIn: 'root'
})

export class ActividadesService {



  constructor(private http: HttpClient,
    private toastr: ToastrService) { }

  urlActividad = 'http://localhost:8080/RutasArgentinas/rest/actividades';

  private usuario: { id: Number, name: string };


  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log('ActividadesService: ' + message);
  }


  getActividades(): Observable<Actividad[]> {
    console.log('Se consultaron las actividades');
    return this.http.get<Actividad[]>(this.urlActividad).pipe(
      catchError((err: any) => { return of([]) }));
  }

  /*getActividadesHabilitadas(): Observable<Actividad[]> {
    console.log('Se consultaron las actividades');
    return this.http.get<Actividad[]>(this.urlActividad + '/habilitadas').pipe(
      catchError((err: any) => { return of([]) }));
  }*/

  getActividadesHabilitadas(): Observable<RespuestaJSON> {
    console.log('Se consultaron las actividades');
    return this.http.get<RespuestaJSON>(this.urlActividad + '/habilitadas').pipe(
      tap(actividad => this.log(`fetched Actividades`)),
      catchError(this.handleError<RespuestaJSON>('getActividadesHabilitadas id=${id}')));
  }

  getUnaActividad(id: Number) {
    console.log('Se consulto la actividad ' + id);
    const url = `${this.urlActividad}/${id}`;
    return this.http.get(url).pipe(
      catchError((err: any) => { return of() }));
  }

  addActividad(actividadRequest: Actividad): Observable<{}> {
    console.log('entro al addActividad con la url guardarActividad');
    const url = this.urlActividad + '/guardarActividad';
    return this.http.post<string>(url, actividadRequest, httpOptions)
      .pipe(
        map(
          res => {
            return res; // JSON.parse(res.toString());
          }
        ),
        catchError(this.handleError<Actividad>('guardarActividad actividad=${actividad}'))
      );
  }

  deleteActividad(id: Number): Observable<{}> {
    const url = `${this.urlActividad}/borrarActividad/${id}`; // ` comilla que se usaba antes
    // const url = this.urlActividad + '/borrarActividad/' + id;
    return this.http.delete(url).pipe(
      // map(_ => this.log(`deleted activity id=${id}`)),
      map(
        res => {
          console.log(res);
          // return JSON.parse(res.toString());
          return res;
        }
      ),
      catchError(this.handleError('deleteActivity'))
    );
  }

  modificarActividad(actividad: Actividad) {
    const urlModificar = this.urlActividad; // + '/Modificar';
    return this.http.put<string>(urlModificar, actividad).pipe(map(
      res => {
        // return JSON.parse(res.toString());
        return res;
      }
    ));
  }

  /*addActividad(actividad: Actividad): Observable<Actividad> {
    console.log('entro al addActiviad');
    return this.http.post<Actividad>(this.urlActividad, actividad, httpOptions).pipe(
      tap((activCreada: Actividad) => this.log(`Se Registró Correctamente La Actividad:` + activCreada.nombre)),
      catchError(this.handleError<Actividad>('addActividad'))
    );
  }*/

  /*postActividad(unaActividad: Actividad): Observable<Actividad> {
    return this.http.post(this.urlActividad + '/guardarActividad', JSON.stringify(unaActividad), httpOptions)
        .map(res => res.json())
        .catch(err => Observable.throw(err));
  }*/

  // function for fetching headers, you can define according to your need over here..
  /*getCommonHeaders() {
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return headers;
  }*/

  /*const url = `${this.urlActividad}/${actividad}`;

   return this.http.post<Actividad>(url, httpOptions).pipe(
     tap(_ => this.log(`Se Registro Correctamente al Usuario`)),
     catchError(this.handleError<Actividad>('addActividad'))
   );*/
}
