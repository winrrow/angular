import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { map } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { Dificultad } from './dificultad';

const headers = new HttpHeaders({
  'Content-Type': 'application/json; charset=utf-8'
});

const httpOptions = {
  headers: headers
};

/*new HttpHeaders({
    'Content-Type': 'application/json',
    'Authorization': 'my-auth-token',
    'Access-Control-Allow-Origin' : '*',
    'Access-Control-Allow-Headers': '*',
    'Access-Control-Allow-Origin': '*',
    'Access-Control-Allow-Credentials': 'true',
    'Access-Control-Allow-Methods': 'GET, PUT, POST, DELETE, HEAD, OPTIONS'
  })*/

const httpOptionsDelete = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': 'localhost:8080',
    'Authorization': 'my-auth-token',
  })
};

@Injectable({
  providedIn: 'root'
})

export class DificultadesService {

  constructor(private http: HttpClient,
    private toastr: ToastrService) { }

    urlDificultad = 'http://localhost:8080/RutasArgentinas/rest/dificultades';

  getDificultades(): Observable<Dificultad[]> {
    console.log('Se consultaron los grados de dificultad');
    return this.http.get<Dificultad[]>(this.urlDificultad).pipe(
      catchError((err: any) => { return of([]) }));
  }


}
