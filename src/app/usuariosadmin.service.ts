import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Usuario } from '../app/usuario';
import { of } from 'rxjs';

const headers = new HttpHeaders({
  'Content-Type': 'application/json; charset=utf-8'
});

const httpOptions = {
  headers: headers
};

@Injectable({
  providedIn: 'root'
})
export class UsuariosadminService {

  constructor(private http: HttpClient) { }

  urlUsuarios = 'http://localhost:8080/RutasArgentinas/rest/usuarios/administradores';

  private usuario: {id: Number, name: string};

 getUsers (): Observable<Usuario[]> {
 return this.http.get<Usuario[]>(this.urlUsuarios).pipe(catchError((err: any) => {return of([])}));}

  getUser(id: number): Observable<Usuario> {
    const url = '${this.urlUsuarios}/${id}';
    return this.http.get<Usuario>(url).pipe(
      tap(usuario => this.log(`fetched hero id=${id}`)),
      catchError(this.handleError<Usuario>('getUser id=${id}'))
    );
  }

  addUser(user: Usuario): Observable<Usuario> {
    console.log('entro al addUser');
    return this.http.post<Usuario>(this.urlUsuarios, user, httpOptions).pipe(
      tap((user: Usuario) => this.log(`Se Registro Correctamente al Usuario`)),
      catchError(this.handleError<Usuario>('addUser'))
    );
  }

  deleteUser (id: Number): Observable<Usuario> {
    const url = `${this.urlUsuarios}/${id}`;

    return this.http.delete<Usuario>(url, httpOptions).pipe(
      tap(_ => this.log(`deleted user id=${id}`)),
      catchError(this.handleError<Usuario>('deleteUser'))
    );
  }

  updateUser (user: Usuario): Observable<null> {
    return this.http.put(this.urlUsuarios, user, httpOptions).pipe(
      tap(_ => this.log(`updated user id=${this.usuario.id}`)),
      catchError(this.handleError<any>('updateUser'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log('UserService: ' + message);
  }
}
