import {Actividad} from '../actividad';
import {ActividadesService} from '../actividades.service';
import {Component, OnInit} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-actividades',
  templateUrl: './actividades.component.html',
  styleUrls: ['./actividades.component.css'],
  providers: [ActividadesService]
})

export class ActividadesComponent implements OnInit {
  actividades: Actividad[] = new Array();
  nuevaActividad: FormGroup;

  constructor(private actividadesService: ActividadesService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService) {}
  ngOnInit() {
    this.getActividades();
     this.nuevaActividad = this.formBuilder.group({
            nombre: ['', Validators.required]
        });
  }

  get f() {return this.nuevaActividad.controls; }

   agregarNuevaActividad() {
    console.log('entroAlAgregarActividad');
    /*this.actividadesService.addActividad(this.nuevaActividad.value).pipe(first())
      .subscribe(actividades => this.getActividades());*/
      this.actividadesService.addActividad(this.nuevaActividad.value).pipe(first())
      .subscribe(actividades => this.getActividades());
  }


  getActividades(): void {
    this.actividadesService.getActividades().subscribe((actividades: any[] ) => {
      console.log(actividades);
      this.actividades = actividades;
  });
  }

borrarActividad(actividadABorrar: Actividad): void {
      this.actividadesService.deleteActividad(actividadABorrar.id).subscribe(confirmacion => this.getActividades());
  }

  modificarActividad(actividadAModificar: Actividad): void {
    this.actividadesService.deleteActividad(actividadAModificar.id).subscribe(confirmacion => this.getActividades());
}


}
