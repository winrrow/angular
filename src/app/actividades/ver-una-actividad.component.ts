import { Component, OnInit } from '@angular/core';
import { ActividadesService } from '../actividades.service';
import { Toast, ToastRef, ToastrService } from 'ngx-toastr';
import { Actividad } from '../actividad';
import { Form, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Params, ActivatedRoute, Router } from '@angular/router';
import { RespuestaJSON } from '../RespuestaJSON';

@Component({
  selector: 'app-ver-una-actividad',
  templateUrl: './ver-una-actividad.component.html',
  styleUrls: ['./ver-una-actividad.component.css']
})
export class VerUnaActividadComponent implements OnInit {
  actividad: Actividad;
  formActividadVisualizada: FormGroup;
  formRadios: FormGroup;
  idAct: Number;
  valorRadio: any;

  constructor(private actividadesService: ActividadesService,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.route.params.subscribe((params: Params) => {
      this.idAct = params['idActividad'];
    });
    this.actividadesService.getUnaActividad(this.idAct).subscribe((actividad: Actividad) => {
      console.log(actividad);  // <-- esta variable es el objeto
      this.actividad = actividad;
      this.valorRadio = actividad.habilitado ? 'Habilitada' : 'Deshabilitada';
      this.formActividadVisualizada = this.formBuilder.group({
        nombre: [this.actividad.nombre], // , Validators.required, Validators.minLength(6)],
        habilitado: [this.valorRadio, Validators.required]
      });
    });

    /* this.formRadios = this.formBuilder.group({
       gender: ['', Validators.required]
     });*/
  }

  getActividad(id: Number): void {
    this.actividadesService.getUnaActividad(id).subscribe((actividad: Actividad) => {
      console.log(actividad);
      this.actividad = actividad;
      this.valorRadio = actividad.habilitado ? 'Habilitada' : 'Deshabilitada';

      this.formActividadVisualizada = this.formBuilder.group({
        nombre: [this.actividad.nombre], // , Validators.required, Validators.minLength(6)],
        habilitado: [this.valorRadio, Validators.required]
      });
    });
  }

  modificarDatosActividad() { // actividad: Actividad) {
    this.actividad.nombre = this.formActividadVisualizada.value.nombre;
    this.actividad.habilitado = this.formActividadVisualizada.value.habilitado === 'Habilitada' ? true : false;
    this.actividadesService.modificarActividad(this.actividad)
      .subscribe((confirmacion) => {
        this.getActividad(this.actividad.id);
        this.toastr.success('Se modifico la actividad');
      });
  }

  borrarActividad(): void {
    this.actividadesService.deleteActividad(this.actividad.id).subscribe(
      confirmacion => this.router.navigate(['/actividades']));
  }
}
