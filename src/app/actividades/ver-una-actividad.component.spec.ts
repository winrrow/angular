import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerUnaActividadComponent } from './ver-una-actividad.component';

describe('VerUnaActividadComponent', () => {
  let component: VerUnaActividadComponent;
  let fixture: ComponentFixture<VerUnaActividadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerUnaActividadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerUnaActividadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
