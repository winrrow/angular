import { Time } from '@angular/common';
import { Actividad } from './actividad';

export class Ruta {
  id: number;
  actividad: Actividad;
  descripcion: string;
  dificultad: string;
  distancia: number;
  fechaRealizacion: string;
  formato: string;
  habilitado: boolean;
  nombre: string;
  privacidad: string;
  recorrido: string; // private Recorrido recorrido;
  tiempoEstimado: Time;
  // foto: any[];
}
