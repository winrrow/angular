import { RouterModule, Routes } from '@angular/router';
import { RegistrarUsuarioComponent } from './registrarusuario/registrarusuario.component';
// import { PanelAdminComponent } from './heroes/heroes.component';
import { ActividadesComponent  } from './actividades/actividades.component';
import { VerUnaActividadComponent  } from './actividades/ver-una-actividad.component';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { ClickMeComponent } from './click-me/click-me.component';
import { EnconstruccionComponent } from './enconstruccion/enconstruccion.component';
import { RecorridosComponent } from './recorridos/recorridos.component';
import { RutasComponent } from './rutas/rutas.component';
import { PaginaInicialComponent } from './pagina-inicial/pagina-inicial.component';
import { NuevaRutaComponent } from './rutas/nueva-ruta/nueva-ruta.component';

export const classRoutes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'nuevoUsuario/:admin', component: RegistrarUsuarioComponent },
  { path: 'usuarios', component: UsuariosComponent },
  { path: 'click', component: ClickMeComponent },
  { path: 'actividades', component: ActividadesComponent},
  { path: 'actividades/:idActividad', component: VerUnaActividadComponent},
  { path: 'rutas', component: RutasComponent},
  { path: 'rutas/:id', component: RutasComponent},
  { path: 'nuevaRuta/:idUsuario', component: NuevaRutaComponent},
  { path: 'recorridos', component: RecorridosComponent},
  { path: 'enConstruccion', component: EnconstruccionComponent },
  { path: 'home', component: PaginaInicialComponent }
];
