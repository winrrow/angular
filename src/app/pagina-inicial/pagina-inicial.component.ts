import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
// import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder, Validators} from '@angular/forms';
import { CookieService } from 'ngx-cookie-service';
import { RutasService } from '../rutas.service';
import { MapInfoWindow, MapMarker } from '@angular/google-maps';


@Component({
  selector: 'app-pagina-inicial',
  templateUrl: './pagina-inicial.component.html',
  styleUrls: ['./pagina-inicial.component.css'],
  styles: [`
    agm-map {
      height: 300px;
    }
  `],
  template: `
  <agm-map [latitude]="lat" [longitude]="lng"></agm-map>
  `
})


export class PaginaInicialComponent implements OnInit {
  lat: number = 51.678418;
  lng: number = 7.809007;
closeResult: string;
nombreUsuario: string;
posteos: any[];
embargoViewModelListEnable: any[];
embargoViewModelListDisable: any[];
// Configuración de Google Maps
center = {lat: -34.903457, lng: -57.938034};
zoom = 15;
display?: google.maps.LatLngLiteral;

posteo1 = {
  id: 23,
  nombre: 'Recorrido de dia por la ruta de los 7 lagos',
  descripcion: 'Linda ruta, se puede hacer a pie o en bicicleta.',
  fechaPublicacion: 'Ayer a las 17:57',
  dificultad: 'Facil',
  distancia: '2 kms',
  fechaRealizacion: '13/01/2020',
  formato: 'ni idea',
  habilitado: true,
  tiempoEstimado: '5mins',
  actividad: [1, 2],
  privacidad: 'publica',
  kml: 'https://www.dropbox.com/s/3bndf59gz24aokg/PO_0800.kml?dl=1',
  usuarioRealizoPosteo: 'Matias',
  comentarios: [{texto: 'Muy buena ruta', fecha: '15/09/2020', horario: '15:00', nombreUsuario: 'Anonimo' },
  {texto: 'Me hubiera encantado poder hacerla', fecha: '10/10/2020', horario: '19:14', nombreUsuario: 'Fermin' },
  {texto: 'Me parece malisima', fecha: '13/11/2020', horario: '05:23', nombreUsuario: 'Alberto Fernandez' }
  ]
};
posteo2 = {
  id: 28,
  nombre: 'Recorrido de dia por Centro de La Plata',
  descripcion: 'Ruta para hacer a pie.',
  fechaPublicacion: 'El Martes a las 12:33',
  dificultad: 'Facil',
  distancia: '5 kms',
  fechaRealizacion: '11/11/2020',
  formato: 'ya veremos',
  habilitado: true,
  tiempoEstimado: '1 hora',
  actividad: [1],
  privacidad: 'publica',
  kml: 'https://developers.google.com/kml/documentation/KML_Samples.kml?hl=es-419',
  usuarioRealizoPosteo: 'Joaco',
  puntajes: [{texto: 'Excelente', fecha: '25/10/2020', horario: '20:00', nombreUsuario: 'Anto' },
  {texto: 'Me perdí en la segunda diagonal', fecha: '28/10/2020', horario: '14:03', nombreUsuario: 'Carmelo' },
  {texto: 'Mi ciudad natal', fecha: '30/10/2020', horario: '16:58', nombreUsuario: 'Cristina Fernandez' }
  ]
};

/* <!-- actividad: null
descripcion: null
dificultad: null
distancia: null
fechaRealizacion: 0
formato: null
habilitado: true
id: 1
kml: null
nombre: "Jujuy"
privacidad: null
tiempoEstimado: null -->*/

  constructor(
    private router: Router,
    private modalService: NgbModal,
    private formBuilder: FormBuilder,
    private cookie: CookieService,
    private rutasService: RutasService
    ) { }

  ngOnInit() {
    this.nombreUsuario = this.cookie.get('nombreUsuario');
    this.rutasService.getUltimasRutas().subscribe((data: any) => {
       console.log(data);
       this.posteos = data;  // datos de la base
       this.posteos[1] = this.posteo1;
       this.posteos[0] = this.posteo2;
    });
  }

  open(content) {
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
      this.closeResult = `Closed with: ${result}`;
    }, (reason) => {
      this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
    });
  }

  placeMarker(algo) {

  }

   private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }


}
