import {Ruta} from './ruta';
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {of} from 'rxjs';
import { RespuestaJSON } from './RespuestaJSON';
import { ToastrService } from 'ngx-toastr';

const headers = new HttpHeaders({
  'Content-Type': 'application/json; charset=utf-8'
});

const httpOptions = {
  headers: headers
};


@Injectable({
  providedIn: 'root'
})
export class RutasService {

  constructor(private http: HttpClient,
              private toastr: ToastrService) {}

  urlRutas = 'http://localhost:8080/RutasArgentinas/rest/rutas';


// RESPUESTA JSON TIENE LO SIGUIENTE
//    Ok: boolean;  <-- Indica si se ejecuto con exito. Si es 0, entonces hay que mostrar el mensaje
//    Value: any;   <-- Si no dio error, en Value se encuentra el objeto que fuimos a buscar.
//    Message: string;   <-- mensaje a mostrar

  getRutasDeUsuario(id: number): Observable<RespuestaJSON> {
    console.log(id);
    const url = this.urlRutas + '/deUsuario/' + id.toString();
    console.log(url);
    return this.http.get<RespuestaJSON>(url).pipe(
      tap(ruta => this.log(`fetched Ruta id=${id}`)),
      catchError(this.handleError<RespuestaJSON>('getRutasDeUsuario id=${id}')));
  }

  getUltimasRutas(): Observable<Ruta> {
    const url = this.urlRutas + '/ultimas';
    // trae las ultimas 10
    console.log(url);
    return this.http.get<Ruta>(url).pipe(
      tap(ruta => this.log(`fetched Rutas`)),
      catchError(this.handleError<Ruta>('getUltimasRutas'))
    );
  }

  addRuta(nuevaRutaRequest: Ruta): Observable<{}> {
    console.log('entro al addRuta');
    console.log(nuevaRutaRequest);
    const url = this.urlRutas + '/guardarRuta';
    return this.http.post<string>(url, nuevaRutaRequest, httpOptions)
      .pipe(
        map(
          res => {
            return res; // JSON.parse(res.toString());
          }
        ),
        catchError(this.handleError<Ruta>('guardarRuta ruta=${ruta}'))
      );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // console.log(error.message);
      this.toastr.error('Ocurrio un error: ' + error.message);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log('UserService: ' + message);
  }
}
