import { TestBed, inject } from '@angular/core/testing';

import { UsuariosadminService } from './usuariosadmin.service';

describe('UsuariosadminService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [UsuariosadminService]
    });
  });

  it('should be created', inject([UsuariosadminService], (service: UsuariosadminService) => {
    expect(service).toBeTruthy();
  }));
});
