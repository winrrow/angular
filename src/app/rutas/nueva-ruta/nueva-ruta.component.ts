import { Component, OnInit } from '@angular/core';
import { Actividad } from 'src/app/actividad';
import { ActividadesService } from 'src/app/actividades.service';
import { Dificultad } from 'src/app/dificultad';
import { DificultadesService } from 'src/app/dificultades.service';
import { RutasService } from 'src/app/rutas.service';
import { ToastrService } from 'ngx-toastr';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import * as moment from 'moment'; // add this 1 of 4
import { fechaATextoJAVA } from 'src/app/NgbDateCustomParserFormatter';

@Component({
  selector: 'app-nueva-ruta',
  templateUrl: './nueva-ruta.component.html',
  styleUrls: ['./nueva-ruta.component.css']
})



export class NuevaRutaComponent implements OnInit {
  actividades: Actividad[] = new Array();
  dificultades: Dificultad[] = new Array();
  formNuevaRuta: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private actividadesService: ActividadesService,
    private dificultadesService: DificultadesService,
    private rutaService: RutasService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
    // cargo el combo de actividades
    /*this.actividadesService.getActividadesHabilitadas().subscribe((actividades: any[] ) => {
      this.actividades = actividades;
      console.log(this.actividades);
    });*/

    // cargo el combo de actividades
    this.actividadesService.getActividadesHabilitadas().subscribe(data => {
      if (data.Ok) {
        this.actividades = data.Value;
        console.log(this.actividades);
        // this.toastr.success('Se consultaron las actividades");
      } else {
        this.toastr.error(data.Message);
      }
    });
    // cargo el combo de dificultad
    this.dificultadesService.getDificultades().subscribe((dificultades: any[] ) => {
      this.dificultades = dificultades;
      console.log(this.dificultades);
    });

    this.formNuevaRuta = this.formBuilder.group({
      nombre: ['Joaquin', Validators.required],
      distancia: ['15', Validators.required],
      dificultad: ['Facil', Validators.required],
      privacidad: ['Publica', Validators.required],
      fechaRealizacion: ['29/05/2020', Validators.required],
      descripcion: ['No lo toma en cuenta', Validators.required],
      actividad: [ , Validators.required],
      formato: ['Circular', Validators.required],
      kml: ['algssso.kml', Validators.required],
      tiempoEstimado: ['7', Validators.required],
      });

  }

  get f() { return this.formNuevaRuta.controls; }

  onSubmitRuta() {
    console.log('entroAlAgregarRuta');
    const formulario = this.formNuevaRuta.value;

    console.log('LA FECHA DEL FORMULARIOOOOOOOOOOOOOOOOOOO');
    console.log(formulario.fechaRealizacion);

    // reasigno la fecha como le gusta a JAVA
    formulario.fechaRealizacion = fechaATextoJAVA(formulario.fechaRealizacion);

    // en el formulario se guarda el ID en el nombre de la actividad, creamos un nuevo objeto actividad y lo volvemos a setear en el form
    var idActividadSeleccionada = formulario.actividad;
    formulario.actividad = new Actividad();
    formulario.actividad.id = idActividadSeleccionada;

    console.log(formulario.actividad);

    // TODO sacar esto  ver como se pasa la actividad completa

    this.rutaService.addRuta(formulario).pipe(first())
      .subscribe(usuarios => this.toastr.success('Ruta cargada correctamente'));
  }

}
