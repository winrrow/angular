import { RutasService } from '../rutas.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { RespuestaJSON } from '../RespuestaJSON';
import { ToastrComponentlessModule, ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-rutas',
  templateUrl: './rutas.component.html',
  styleUrls: ['./rutas.component.css']
})

export class RutasComponent implements OnInit {
  _idUsuario: number;
  rutas: any;
  constructor(private activatedRoute: ActivatedRoute,
    private rutasService: RutasService,
    private router: Router,
    private toastr: ToastrService) { }

  ngOnInit() {
    // obtengo los parametros
    this.activatedRoute.params.subscribe((params: Params) => {
      const idUsuario = params['id'];
      this._idUsuario = idUsuario;
      console.log(idUsuario);

      // busco las rutas del usuario
      this.rutasService.getRutasDeUsuario(this._idUsuario).subscribe(data => {
        if (data.Ok) {

          this.rutas = data.Value;
          console.log(data);
          this.toastr.success('Todo bien con las rutas del usuario');
        } else {
          this.toastr.error(data.Message);
        }
      });

    });
  }
  verRuta(id: any) {
    console.log('SE VISUALIZA LA RUTAAAAAAAAAAAAAAAAAAAAAAAAA');
    console.log(id);
    this.router.navigate(['/enConstruccion']);
  }

  nuevaRutaDeUnUsuario() {
    this.router.navigate(['/nuevaRuta/', this._idUsuario]);
  }

}
