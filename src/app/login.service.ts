import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DatosLogin } from '../app/usuarioLogin';
import { Usuario } from '../app/usuario';
import { of } from 'rxjs';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { RespuestaJSON } from './RespuestaJSON';

const httpOptions = {
 headers: new HttpHeaders({
 'Content-Type': 'application/json',
 'Authorization': 'my-auth-token'
 })
};

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(private http: HttpClient, private toastr: ToastrService) { }

  urlLogin = 'http://localhost:8080/RutasArgentinas/rest/usuarios/login';

  /*checkUser(user: DatosLogin): Observable<Usuario> {
    console.log(this.urlLogin + '/' + user.usuario.toString() + '/' + user.password.toString());
    return this.http.get<Usuario>(this.urlLogin + '/' + user.usuario.toString() + '/' + user.password.toString()).pipe(
      tap(usuario => this.log('fetched user log id=${id}')),
      catchError(this.handleError<Usuario>('getUser id=${id}'))
    );
  }*/

  checkUser(user: DatosLogin): Observable<RespuestaJSON> {
    console.log(this.urlLogin + '/' + user.usuario.toString() + '/' + user.password.toString());
    return this.http.get<RespuestaJSON>(this.urlLogin + '/' + user.usuario.toString() + '/' + user.password.toString()).pipe(
      map(
        res => {
          console.log(res);
          return res; // JSON.parse(res.toString());
        }
      ),
      catchError(this.handleError<RespuestaJSON>('checkUsuario usuario=${user}'))
    );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);
      return of(result as T);
    };
  }

  private log(message: string) {
    console.log('UserService: ' + message);
  }

}
