import {Usuario} from '../usuario';
import {Component, OnInit} from '@angular/core';
import {UsuariosService} from '../usuarios.service';
import {FormsModule, ReactiveFormsModule, FormGroup, FormBuilder, Validators} from '@angular/forms';
import {first} from 'rxjs/operators';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, Params } from '@angular/router';
import { UsuariosadminService } from '../usuariosadmin.service';


@Component({
  selector: 'app-registrarusuario',
  templateUrl: './registrarusuario.component.html',
  styleUrls: ['./registrarusuario.component.css'],
  providers: [UsuariosService]
})
export class RegistrarUsuarioComponent implements OnInit {
  formularioDeRegistroAdmin: FormGroup;
  formularioDeRegistroComun: FormGroup;
  // labelPagina = '';
  _esAdmin = false;

  constructor(
    private formBuilder: FormBuilder,
    private usuariosComunService: UsuariosService,
    private usuariosAdminService: UsuariosadminService,
    private route: ActivatedRoute,
    private toastr: ToastrService) {}

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this._esAdmin = params['admin'] === '1';
    });

    if (this._esAdmin) {
    this.formularioDeRegistroAdmin = this.formBuilder.group({
        nombre: ['', Validators.required],
        apellido: ['', Validators.required],
        email: ['', Validators.required],
        fechaDeNacimiento: ['01/01/1980', Validators.required],
        nombreUsuario: ['', Validators.required],
        password: ['', [Validators.required, Validators.minLength(6)]],
        });
      } else {
        this.formularioDeRegistroComun = this.formBuilder.group({
          nombreUsuario: ['', Validators.required],
          email: ['', Validators.required],
          password: ['', [Validators.required, Validators.minLength(6)]],
          });
      }
  }

  get f() { return this.formularioDeRegistroAdmin.controls; }

  get e() { return this.formularioDeRegistroComun.controls; }

  onSubmitAdmin() {
    console.log('entroAlregistrarUsuarioAdmin');
    console.log(this.formularioDeRegistroAdmin.value);

    this.usuariosAdminService.addUser(this.formularioDeRegistroAdmin.value).pipe(first())
      .subscribe(usuarios => this.toastr.success('Usuario ADMIN se grabo correctamente'));
  }

  onSubmitComun() {
    console.log('entroAlregistrarUsuario');
    this.usuariosComunService.addUser(this.formularioDeRegistroComun.value).pipe(first())
      .subscribe(usuarios => this.toastr.success('Usuario COMUN se grabo correctamente'));
  }

}
