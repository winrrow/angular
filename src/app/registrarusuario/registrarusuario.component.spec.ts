import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UnusuarioComponent } from './unusuario.component';

describe('UnusuarioComponent', () => {
  let component: UnusuarioComponent;
  let fixture: ComponentFixture<UnusuarioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UnusuarioComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UnusuarioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
