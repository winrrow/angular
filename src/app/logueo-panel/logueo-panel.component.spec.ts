import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LogueoPanelComponent } from './logueo-panel.component';

describe('LogueoPanelComponent', () => {
  let component: LogueoPanelComponent;
  let fixture: ComponentFixture<LogueoPanelComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LogueoPanelComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LogueoPanelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
