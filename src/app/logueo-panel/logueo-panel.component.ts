import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LoginService } from '../login.service';
import { first } from 'rxjs/operators';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { CookieService } from 'ngx-cookie-service';
import { ToastrService, IndividualConfig } from 'ngx-toastr';

@Component({
  selector: 'app-logueo-panel',
  templateUrl: './logueo-panel.component.html',
  styleUrls: ['./logueo-panel.component.css']
})
export class LogueoPanelComponent implements OnInit {// implements AfterViewInit{

  usuarioAdmin: boolean;
  mostrarLogeo: boolean;
  nombreUsuario: string;
  formularioDeLogin: FormGroup;
  _idUsuario: number;
  options: IndividualConfig;

  constructor(private loginService: LoginService,
    private formBuilder: FormBuilder,
    private router: Router,
    private cookie: CookieService,
    private toastr: ToastrService
  ) {
    this.options = this.toastr.toastrConfig;
    this.options.positionClass = 'toast-top-right';
    this.options.timeOut = 10000;
  }

  ngOnInit() {
    // this.toastr.success('Hola mundo cruel', 'me faltaba el titulo');
    this.nombreUsuario = 'vacio';
    const idUsuario = this.cookie.get('id');
    if (idUsuario) {
      this.mostrarLogeo = false; // oculto
      this.usuarioAdmin = this.cookie.get('perfil') === 'Admin';
      this.nombreUsuario = this.cookie.get('nombre');
    } else {
      this.blanquearValores();
    }
  }

  blanquearValores() {
    this.mostrarLogeo = true;
    // this.usuarioAdmin = false;
    this.formularioDeLogin = this.formBuilder.group({
      usuario: ['', Validators.required],
      password: ['', [Validators.required, Validators.minLength(6)]]
    });
  }

   mostrarToastrVerde() {
     this.toastr.success('Hola mundo cruel', 'me faltaba el titulo');
   }

  /* mostrarToastrVerde() {
    this.toastr.success('Hello world!', 'Toastr fun!', {
      disableTimeOut: true
    });
  } */

 /*pipe(
    first(val => val > 6),
  ).subscribe(console.log, err => console.log('Error', err));*/

  /*ingresoDeUsuario() {    //ESTE RECIBIA UN OBJETO USUARIO
    this.loginService.checkUser(this.formularioDeLogin.value).pipe(first(val => val !== undefined ))
      .subscribe(usuarioDeLaBase => (
        console.log(usuarioDeLaBase),
        this.cookie.set('id', usuarioDeLaBase.id.toString()),
        this.cookie.set('pass', usuarioDeLaBase.password.toString()),
        this.cookie.set('nombreUsuario', usuarioDeLaBase.nombreUsuario.toString()), // para el saludo
        this.cookie.set('perfil', usuarioDeLaBase.perfil.toString()), // para el saludo
        this._idUsuario = usuarioDeLaBase.id,
        this.usuarioAdmin = (usuarioDeLaBase.perfil === 'Admin'),
        this.mostrarLogeo = false));
  }*/

  ingresoDeUsuario() {  // ESTE RECIBE UN OBJETO RESPUESTA JSON
    this.loginService.checkUser(this.formularioDeLogin.value)  // pipe(first(val => val !== undefined )) SACO ESTO
      .subscribe(data => {
        if (data.Ok) {
          console.log(data);
          this.cookie.set('id', data.Value.id.toString());
          this.cookie.set('pass', data.Value.password.toString());
          this.cookie.set('nombreUsuario', data.Value.nombreUsuario.toString()); // para el saludo
          this.cookie.set('perfil', data.Value.perfil.toString()); // para el saludo
          this._idUsuario = data.Value.id;
          this.usuarioAdmin = (data.Value.perfil === 'Admin');
          this.mostrarLogeo = false;
          this.toastr.success('Todo bien con las rutas del usuario');
        } else {
          this.toastr.error(data.Message);
        }
      });
  }

  cerrarSesion() {
    this.cookie.deleteAll();
    this.blanquearValores();
    this.router.navigate(['/home']);
  }

  /*verMisRutas() {
    this.router.navigate(['/rutas/', this._idUsuario]);
  }

  irAlHome() {
    this.router.navigate(['/home']);
  }*/

}
