export class Usuario {
  id: number;
  nombre: string;
  apellido: string;
  fechaAlta: Date;
  password: string;
  fechaDeNacimiento: Date;
  nombreUsuario: string;
  email: string;
  habilitado: boolean;
  perfil: string;
}
