import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { RouterModule, Routes } from '@angular/router';
import { classRoutes } from './routes';
import { AppComponent } from './app.component';
import { ClickMeComponent } from './click-me/click-me.component';
import { RegistrarUsuarioComponent } from './registrarusuario/registrarusuario.component';
import { UsuariosService } from './usuarios.service';
import { UsuariosadminService } from './usuariosadmin.service';
import { UsuariosComponent } from './usuarios/usuarios.component';
import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { EncabezadoComponent } from './encabezado/encabezado.component';
import { PiedepaginaComponent } from './piedepagina/piedepagina.component';
import { EnconstruccionComponent } from './enconstruccion/enconstruccion.component';
import { ActividadesComponent } from './actividades/actividades.component';
import { RutasComponent } from './rutas/rutas.component';
import { RecorridosComponent } from './recorridos/recorridos.component';
import { PaginaInicialComponent } from './pagina-inicial/pagina-inicial.component';
import { CookieService } from 'ngx-cookie-service';
import { NgbPaginationModule, NgbAlertModule, NgbModule, NgbModalModule, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { LogueoPanelComponent } from './logueo-panel/logueo-panel.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ngx-toastr';
import { GoogleMapsModule } from '@angular/google-maps';
import { AgmCoreModule } from '@agm/core';
import { VerUnaActividadComponent } from './actividades/ver-una-actividad.component';
import { NgbDateCustomParserFormatter } from './NgbDateCustomParserFormatter';
import { NuevaRutaComponent } from './rutas/nueva-ruta/nueva-ruta.component';


/*
 * USO EL ARCHIVO ROUTES.ts
 * const routes: Routes = [
  { path: '', redirectTo: '/primeraAppAngular', pathMatch: 'full' },

  { path: 'primeraAppAngular', component: AppComponent },
  { path: 'usuario/:id', component: UnusuarioComponent },
  { path: 'usuarios', component: UsuariosComponent },*/
 /* { path: 'click', component: ClickMeComponent }
];*/

@NgModule({
  declarations: [
    AppComponent,
    ClickMeComponent,
    RegistrarUsuarioComponent,
    UsuariosComponent,
    EncabezadoComponent,
    PiedepaginaComponent,
    EnconstruccionComponent,
    ActividadesComponent,
    RutasComponent,
    RecorridosComponent,
    PaginaInicialComponent,
    LogueoPanelComponent,
    VerUnaActividadComponent,
    NuevaRutaComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(classRoutes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    NgbModule,
    NgbModalModule,
    NgbPaginationModule,
    NgbAlertModule,
    GoogleMapsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDzMpCmIJubP7OV0tdOLuZ7Zz3ZIlNuKBs'
    })
  ],
  providers: [UsuariosService, UsuariosadminService, HttpClient, CookieService,
    {provide: NgbDateParserFormatter, useClass: NgbDateCustomParserFormatter}],
  bootstrap: [AppComponent]
})
export class AppModule { }
