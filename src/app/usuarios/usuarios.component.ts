import {Component, OnInit} from '@angular/core';
import {Usuario} from '../usuario';
import {UsuariosService} from '../usuarios.service';
import {UsuariosadminService} from '../usuariosadmin.service';

@Component({
  selector: 'app-usuarios',
  templateUrl: './usuarios.component.html',
  styleUrls: ['./usuarios.component.css'],
  providers: [UsuariosService]
})

export class UsuariosComponent implements OnInit {
  usuarios: Usuario[];
  usuariosAdmins: Usuario[];

  constructor(private usuariosService: UsuariosService, private servicioUsuariosAdmin: UsuariosadminService) {}
  ngOnInit() {
    this.getUsers();
  }

  getUsers(): void {
    this.usuariosService.getUsers().subscribe(usuarios => this.usuarios = usuarios);
    this.servicioUsuariosAdmin.getUsers().subscribe(usuarios => this.usuariosAdmins = usuarios);
  }

  cambiarEstadoUsuario(usuarioAModificar: Usuario): void {

    if (usuarioAModificar.habilitado) {
      this.usuariosService.deleteUser(usuarioAModificar.id).subscribe(confirmacion => this.getUsers());
    } else {
      usuarioAModificar.habilitado = true;
      this.usuariosService.updateUser(usuarioAModificar).subscribe(confirmacion => this.getUsers());
    }
  }

  cambiarEstadoUsuarioAdmin(usuarioAModificar: Usuario): void {
    if (usuarioAModificar.habilitado) {
      this.servicioUsuariosAdmin.deleteUser(usuarioAModificar.id).subscribe(confirmacion => this.getUsers());
    } else {
      usuarioAModificar.habilitado = true;
      this.servicioUsuariosAdmin.updateUser(usuarioAModificar).subscribe(confirmacion => this.getUsers());
    }
  }
}
